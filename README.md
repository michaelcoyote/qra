# Quick Reference Archetecture

## About the name Quick Reference Architecture (QRA)

A [Reference Model](https://en.wikipedia.org/wiki/Reference_model) WRT systems engineering is an abstract framework used to educate people about the components of an environment and their relationships between components in an environment.

## Goals and Puropose

This is an implementation of Kubernetes using infrastructure as code tools as a way to quickly deploy a k8s multi-node envrionemnt for testing and learning purposes.

A secondary goal of this is for me to better understand Kubernetes concepts as well as IaC tools.

This is not a tool for installing a production environment.

## Limitations

- At this time only AWS is supported.
- We will only be using one region.
- This is not intended for production use.


## Resources

- [Kubernetes Cluster Architecture](https://kubernetes.io/docs/concepts/architecture/): The base documentation from the kubernetes project
- [Kubernetes The Hard Way](https://github.com/kelseyhightower/kubernetes-the-hard-way): The definiative guide on setting up Kubernetes in a learning environment
